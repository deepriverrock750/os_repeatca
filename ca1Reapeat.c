#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define SIZE 256
struct TLB{
	 	int frame;
	 	int address;
	 	char content[10];
 	};
/*struct PhysicalMemory{
	int frameNum;
	int frameAddr;
	char content[10];
	int inUse;
};

struct PageTable{
	int pageAddr;
	int pageNum;
	int pageOffset;
	int frameNum;
};*/

int main()
{
	srand(time(0));
	//Variables
	int virtualAddress = 0x5D4A, physicalAddress = 0x2E4A;
	int TLB_SIZE = 9;
	struct TLB tlb[8];
	int i=0, j=0;

	//virtual Memory
	int page;		//PageNumber
	int d;			// page-offset

	//Physical memory
	int frame = 0; 	//frameNumber
	int offset;



	//Write To File // Physical Memory
	FILE * writePhysicalMemory;
	writePhysicalMemory = fopen("data/physical_memory.txt", "w+");
	fprintf(writePhysicalMemory, "\tAddress\t|\tFrames\t|\tContent\t\t|\tIn Use\n");
							fprintf(writePhysicalMemory,"__________________________________________________________________________________________________\n");

	int inUse;
	for(i=0; i <= SIZE- 1; i++)
	{
		char temp[10];
		inUse = 0;
		physicalAddress += i;

		for (j = 0; j <= 10; ++j)
		{
			temp[j] = rand() % 26 + 'A';
		}
		temp[j] = '\0';

		if(i < TLB_SIZE)
		{
			tlb[i].address = physicalAddress;
			tlb[i].frame = frame;
			for(int k = 0; k <= 10; k++)
			{
				tlb[i].content[k] = temp[k]; 
			}
			inUse = 1;
		}

		// add addresses to tlb and save with ones are added ie like 5
		// picks randomly if picked set 1 else 0
		fprintf(writePhysicalMemory, "\t0x%x\t|\t%d\t|\t%s\t|\t%d\n", 			physicalAddress, frame, temp, inUse);
		frame += 1;
	}
	
	/*for(int a=0; a <=8; a++)
	{
		printf("Frame: %d\nPhysical:%x\nContent:%s\n", tlb[a].frame, 			tlb[a].address, tlb[a].content);
	}*/
	
	fclose(writePhysicalMemory);

	
	// Write page table to file
	int pn;
	unsigned int po;
	unsigned int offsetMask = 0x00FF;

	FILE * writePageTable;
	writePageTable = fopen("data/page_table.txt", "w+");
	fprintf(writePageTable, "\tPage Address\t|\tPage Number\t| Page Offset | Frame Number\n");
	fprintf(writePageTable,"_______________________________________________________________________________________\n");
	
	int oddFrame=1;
	int evenFrame=0; 
	int ptframe;
	for(i=0; i <= SIZE- 1; i++)
	{
		
		if(i % 2 == 0)
		{
			ptframe = oddFrame;
			oddFrame += 2;
		}		
		else 
		{
			ptframe = evenFrame;
			evenFrame += 2;
		}

		po = virtualAddress & offsetMask;
		pn = virtualAddress >> 8;
		fprintf(writePhysicalMemory, "\t0x%x\t\t|\t\t%x\t|\t%02x\t|\t%d\t\n", 			virtualAddress, pn, po, ptframe);
		
		virtualAddress += i;
	}

	fclose(writePageTable);

	//getting Input
	unsigned int input_address;
	printf("Please enter a virtual Memory Address FROM the page table in Hex Form (16 bits)\n0x");
	scanf("%x", &input_address);
	printf("input address: 0x%x", input_address);
	//check whether address is in tlb, if not add it to the tlb
	int tempaddr;
	int tempoffset;
	int inputOffset;
	for(int q = 0; q < TLB_SIZE; q++)
	{
		tlb[q].address = tempaddr;
		tempoffset = tempaddr & offsetMask;
		printf("TLB address Offset: 0x%x", tempoffset);
		inputOffset = input_address & offsetMask;
		printf("input address Offset: 0x%x", inputOffset);
		

		
			
		
	}

}

